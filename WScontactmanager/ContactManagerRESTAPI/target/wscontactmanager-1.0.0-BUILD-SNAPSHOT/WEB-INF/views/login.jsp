<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><spring:message code="label.title" /></title>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="resources/signin.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <form class="form-signin" method="POST" action="<c:url value="/j_spring_security_check" />">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" class="form-control" name="j_username" placeholder="Username" required autofocus>
        <input type="password" class="form-control" name="j_password"  placeholder="Password" required>
        <label class="checkbox">
            <input type="checkbox" name="_spring_security_remember_me" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </form>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
