package com.khomenko.wscontactmanager.service;

import java.util.List;
import com.khomenko.wscontactmanager.domain.Contact;

public interface ContactService {

	public void addContact(Contact contact);

	public List<Contact> listContact();

	public void removeContact(Integer id);
}
