package com.khomenko.wscontactmanager.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.khomenko.wscontactmanager.domain.Contact;
import com.khomenko.wscontactmanager.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@Controller
@RequestMapping(value = "/ws" )
public class ContactWsController {

    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/index", produces ="application/json" )
    @ResponseBody
    private List<Contact> listContacts() {

        return contactService.listContact();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "text/plain")
    @ResponseBody
    public List<Contact> addContactWs(@RequestBody String json) {

            Contact contact = null;
        try {
            contact = new ObjectMapper().readValue(json, Contact.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        contactService.addContact(contact);
        return contactService.listContact();

    }

    @RequestMapping(value ="/delete/{contactId}")
    @ResponseBody
    public List<Contact> deleteContactWs(@PathVariable("contactId") Integer contactId) {

       contactService.removeContact(contactId);
       return contactService.listContact();
    }

}