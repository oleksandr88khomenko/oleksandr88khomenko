package com.khomenko.wscontactmanager.dao;

import java.util.List;
import com.khomenko.wscontactmanager.domain.Contact;

public interface ContactDAO {

	public void addContact(Contact contact);

	public List<Contact> listContact();

	public void removeContact(Integer id);
}