<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><spring:message code="label.title" /></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="resources/processing.js"></script>
    <link href="resources/bootstrap.css" rel="stylesheet">
    <link href="resources/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <a href="<c:url value="/logout" />">
        <spring:message code="label.logout" />
    </a>

    <form class="form-horizontal" method="post" action="" id="contact">
        <div class="control-group">
            <label class="control-label" for="firstname"><spring:message code="label.firstname" /></label>
            <div class="controls">
                <input type="text" id="firstname" name="firstname">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="lastname"><spring:message code="label.lastname" /></label>
                <div class="controls">
                <input type="text" id="lastname" name="lastname" >
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="email"><spring:message code="label.email" /></label>
            <div class="controls">
                <input type="text" id="email" name="email" >
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="telephone"><spring:message code="label.telephone" /></label>
            <div class="controls">
                <input type="text" id="telephone" name="telephone" >
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn"><spring:message code="label.addcontact"/></button>
            </div>
        </div>
    </form>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
