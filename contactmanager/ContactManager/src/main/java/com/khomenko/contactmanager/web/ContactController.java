package com.khomenko.contactmanager.web;

import java.util.HashMap;
import java.util.Map;
import com.khomenko.contactmanager.domain.Contact;
import com.khomenko.contactmanager.service.ContactService;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@Controller
public class ContactController {


	@Autowired
	private ContactService contactService;

	@RequestMapping(value="/contacts",  method = RequestMethod.GET)
	public String contactsPage() {

        return "contact";
	}
	
	@RequestMapping("/")
	public String home() {
		return "login";
	}

    @RequestMapping("/error")
    public String error() {
        return "error403";
    }
}
