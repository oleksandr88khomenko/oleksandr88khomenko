<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><spring:message code="label.title" /></title>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="resources/signin.css" rel="stylesheet">

</head>

<body>

<div class="container">


        <h2 class="form-signin-heading">Unfortunately, you can't have access to manager without permission</h2>
        <a href="<c:url value="/" />">Try one more time</a>


</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
