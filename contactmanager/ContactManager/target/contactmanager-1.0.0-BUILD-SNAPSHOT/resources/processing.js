/**
 * Created by oleksandr_khomenko on 07.12.13.
 */
function printResponse(response) {
    var respContent =
        '<table class="table table-hover">' +
        '<tbody><tr><th>Имя</th><th>Email</th><th>Телефон</th><th>&nbsp;</th></tr>';
    for(var i = 0; i < response.length; i++) {
        respContent += '<tr><td>' + response[i].firstname +' '+ response[i].lastname + '</td>' + '<td>' + response[i].email + '</td><td>' + response[i].telephone + '</td><td><a href="javascript:void(0);" onclick="+ removeContact('+response[i].id+');">Удалить</a></td></tr>';
    }
    respContent += '</tbody>';
    $('.table.table-hover').hide();
    $('.form-horizontal').after(respContent);

}

function removeContact (contactId){

    $.ajax ({

        url: 'http://localhost:8080/WSContactManager/ws/delete/' + contactId,
        type:'GET',
        dataType:"json",
        success: function(response){
            printResponse(response)
        }

    })

}

$(document).ready(function(){

    $.ajax ({

        url: 'http://localhost:8080/WSContactManager/ws/index',
        type:'GET',
        dataType:"json",
        success: function(response){
            printResponse(response)
        }

    })

    $('.delete').click(function() {

        $.ajax({

            url: 'http://localhost:8080/WSContactManager/ws/delete',
            data:  JSON.stringify(json),
            type:'POST',
            dataType: 'json',
            success: function(response) {
                    printResponse(response)
            }
        });

    })

    $('.form-horizontal').submit(function(event){

        var firstname = $('input[name="firstname"]').val();
        var lastname = $('input[name="lastname"]').val();
        var email = $('input[name="email"]').val();
        var telephone = $('input[name="telephone"]').val();
        var json = {"firstname": firstname, "lastname": lastname,"email": email, "telephone": telephone };

        $.ajax({

                    url: 'http://localhost:8080/WSContactManager/ws/add',
                    data:  JSON.stringify(json),
                    type:'POST',
                    dataType: 'json',
                    contentType:"text/plain",
            success: function(response) {
                printResponse(response)
            }
        });
        event.preventDefault();

    });

})